var Botkit = require("botkit");
var XMLHttpRequest = require("xhr2").XMLHttpRequest;
var bot_token = process.argv[2];

var controller = Botkit.slackbot({
    debug: false
});

var titles_cache = [];

// connect the bot to a stream of messages
controller.spawn({
   token: bot_token,
}).startRTM();

controller.hears(['^\\d+$'],['direct_mention'],function(bot,message) {
    bot.say({
        'text': 'Okay, looking for comic #' + message.match[0] + '...',
        'channel': message.channel
    })
    var id = message.match[0];
    send_comic(id,bot,message);
});
controller.hears(['^[A-Za-z0-9 ]+$'],['direct_mention'],function(bot,message) {
    var found_comics = search_comics(message.text);
    console.log("# of found comics: " + found_comics.length);
    if (found_comics.length > 1)
    {
        bot.say({
            'channel': message.channel,
            'text': "Sorry, I found multiple comics with those keywords in the title. Try adding a few more."
        });
    }
    else if (found_comics.length == 0)
    {
        bot.say({
            'channel': message.channel,
            'text': "Sorry, I couldn\'t find that comic."
        });
    }
    else{
        bot.say({
            'channel': message.channel,
            'text': "Found this."
        });
        send_comic(found_comics[0],bot,message);
    }
});
fetch_titles();
function send_comic(id,bot,message)
{
    fetch_titles();
    var got_it = false;
    if (titles_cache[id])
    {
        bot.say({
            'username': 'xkcdbot',
            'text': "Here you go (#" + id + "): ",
            'channel': message.channel,
            'attachments': [
                {
                    'fallback': titles_cache[id].alt,
                    'image_url': titles_cache[id].img
                }
            ]
        });
        got_it = true;
    }
    else{
        bot.say({
            'text': 'Sorry, I couldn\'t find that comic.',
            'channel': message.channel
        })
    }
    return got_it;
}
function fetch_titles()
{
    var xhr = new XMLHttpRequest();
    xhr.open("GET","https://xkcd.com/info.0.json",true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4)
        {
            if (xhr.status == 200 || xhr.status == 304)
            {
                json = JSON.parse(xhr.responseText);
                if (titles_cache.length != json.num)
                {
                    for (var i = titles_cache.length; i <= json.num; i++)
                    {
                        fetch_json_info(i);
                    }
                }
            }
        }
    };
    xhr.send(null);
}
function fetch_json_info(id)
{
    var json = false;
    var url = "";
    if (id)
    {
        url = "https://xkcd.com/" + id + "/info.0.json";
    }
    else{
        url = "https://xkcd.com/info.0.json";
    }
    var xhr = new XMLHttpRequest();
    xhr.open("GET",url,true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4)
        {
            if (xhr.status == 200 || xhr.status == 304)
            {
                console.log("Fetching title for comic #" + id);
                json = JSON.parse(xhr.responseText);
                titles_cache[json.num] = json;
            }
        }
    };
    xhr.send(null);
}
function search_comics(title_keywords)
{
    fetch_titles();
    var found_comics = [];
    for (var i = 1; i < titles_cache.length; i++)
    {
        if (titles_cache[i])
        {
            if (titles_cache[i].title.match(new RegExp(title_keywords.split(" ").join("|"),"i")) != null)
            {
                found_comics.push(i);
            }
        }
    }
    return found_comics;
}